from flask import Flask, request, render_template, redirect
import requests
import os
import json
import csv

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/search", methods=["POST"])
def search():
    """ This function searches for cities and returns a list for user to choose from
    when called by post request """

    city_name=request.form.get("city_name")
    if request.form.get("units") == "metric":
        unit = "metric"
    elif request.form.get("units") == "imperial":
        unit = "imperial"
    city_list = get_city_info(city_name)
    #check for list empty or full
    if len(city_list) == 0:
        return render_template("index.html", Error = "City not found!")
    else:
        # create a list containing city name and id
        info = []
        for city in city_list:
            info.append(dict(name=city.get("name"), id=city.get("id")))
        return render_template("city_search.html", city_list = info, unit = unit)
    
    
@app.route("/search", methods=["GET"])
def return_data():   
    # returns city weather data
    # set required variables safely
    city_id = request.args.get("id")
    #city_name = get_city_info(city_id)
    if request.args.get("unit") == "metric":
        unit = "metric"
    elif request.args.get("unit") == "imperial":
        unit = "imperial"
    
    # get the data
    data = get_data(city_id, unit)
    if data == "KeyError" or data == "Connection Error":
        return "Error, Please try again later."
    return render_template("result.html",temp = data[1], weather_main = data[0], city_name = data[2])

def get_data(city_id, unit):
    """
    Sends a request to openweathermap and receives the data
    """

    try:
        api_key = os.environ["API_KEY"]
    except KeyError:
        return "KeyError"
    url = "https://api.openweathermap.org/data/2.5/weather?id=" + str(city_id) + "&units=" + unit + "&appid=" + api_key
    try:
        response = requests.get(url)
    except:
        return "Connection Error"
    response_json = response.json()
    data = []
    data.append(response_json.get("weather")[0].get("main"))
    data.append(response_json.get("main").get("temp"))
    data.append(response_json.get("name"))

    return data
    
def get_city_info(city_name):
    """Converts city name to city id from openweathermap.
    If provided with an integer(id) returns city name"""

    THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
    city_list = []

    with open(os.path.join(THIS_FOLDER, 'city.list.json'), "r") as city_list_file:
        city_list_json = json.load(city_list_file)
        
        for i in city_list_json:
            if city_name.lower() in i.get("name").lower():
                city_list.append(i)
        return city_list

@app.route("/register", methods=["POST"])
def register():
    THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
    name = request.form.get("name")
    email = request.form.get("email")
    with open(os.path.join(THIS_FOLDER, 'list.csv'), mode="a") as registrants:
        writer = csv.writer(registrants)
        writer.writerow([name, email])
        return "done"


